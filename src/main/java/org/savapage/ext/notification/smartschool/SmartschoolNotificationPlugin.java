/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.notification.smartschool;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.savapage.core.SpException;
import org.savapage.core.template.dto.TemplateJobTicketDto;
import org.savapage.core.template.dto.TemplateUserDto;
import org.savapage.ext.ServerPluginContext;
import org.savapage.ext.ServerPluginException;
import org.savapage.ext.notification.JobTicketCancelEvent;
import org.savapage.ext.notification.JobTicketCloseEvent;
import org.savapage.ext.notification.JobTicketEvent;
import org.savapage.ext.notification.NotificationEventProgress;
import org.savapage.ext.notification.NotificationPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Rijk Ravestein
 *
 */
public final class SmartschoolNotificationPlugin extends NotificationPlugin {

    /**
     * The {@link Logger}.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(SmartschoolNotificationPlugin.class);

    /** */
    private static final String PROP_ACCOUNT = "smartschool.account";

    /** */
    private static final String PROP_PASSWORD = "smartschool.api.password";

    /** */
    private static final String PROP_TEMPLATE_HOME =
            "smartschool.template.home";
    /** */
    private static final String PROP_TEMPLATE_JOBTICKET_CANCEL =
            "smartschool.template.jobticket.cancel";
    /** */
    private static final String PROP_TEMPLATE_JOBTICKET_CLOSE =
            "smartschool.template.jobticket.close";

    /** */
    private static final String PROP_NOTIFICATION_SENDER_GROUP =
            "smartschool.notification.sender.group";

    /** */
    private static final String PROP_NOTIFICATION_RECIPIENT_GROUP =
            "smartschool.notification.recipient.group";

    /** */
    private static final String PROP_NOTIFICATION_EVENT_MAX =
            "smartschool.notification.event.accept.max";

    /** */
    private static final String ENDPOINT_STRING_FORMAT =
            "https://%s.smartschool.be/Webservices/V3";

    /** */
    private static final String QNAME_SEND_MSG = "sendMsg";

    /** */
    private String id;

    /** */
    private String name;

    /** */
    private boolean liveMode;

    /** */
    private ServerPluginContext pluginContext;

    /** */
    private SOAPConnection connection;

    /**
     * SOAP end-point as {@link URL}.
     */
    private URL endpointUrl;

    /**
     * The secret access code for sendMsg.
     */
    private char[] accesscode;

    /** */
    private File xmlResourceHome;

    /** */
    private String xmlResourceJobTicketCancel;

    /** */
    private String xmlResourceJobTicketClose;

    /** */
    private String senderGroup;

    /** */
    private String recipientGroup;

    /** */
    private Integer maxAcceptedEvents;

    /**
     * Events to send.
     */
    private final BlockingQueue<SmartschoolMsg> eventQueue =
            new LinkedBlockingQueue<>();

    /** */
    private EventConsumer eventConsumer;

    /**
     * {@code true} when stop is requested.
     */
    private volatile boolean requestStop = false;

    /** */
    class SmartschoolMsg {

        private String sender;
        private String recipient;
        private String title;
        private String body;

        public String getSender() {
            return sender;
        }

        public void setSender(String sender) {
            this.sender = sender;
        }

        public String getRecipient() {
            return recipient;
        }

        public void setRecipient(String recipient) {
            this.recipient = recipient;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

    }

    /** */
    class EventConsumer extends Thread {

        /** */
        private final SmartschoolNotificationPlugin parent;

        /** */
        private static final long POLL_SECS_TIMEOUT = 2L;

        /** */
        public static final long MAX_MSECS_PROCESSING_AFTER_STOP = 20 * 1000;

        /**
         *
         * @param plugin
         *            The parent plug-in.
         */
        EventConsumer(final SmartschoolNotificationPlugin plugin) {
            super("SmartschoolEventConsumer");
            this.parent = plugin;
        }

        @Override
        public void run() {

            try {
                while (true) {

                    final SmartschoolMsg msg = parent.eventQueue
                            .poll(POLL_SECS_TIMEOUT, TimeUnit.SECONDS);

                    if (msg == null && parent.requestStop) {
                        break;
                    }

                    if (msg != null) {
                        try {
                            parent.sendMsg(msg);
                        } catch (SOAPException e) {
                            LOGGER.error("[{}] sendMsg failed: {}",
                                    this.getId(), e.getMessage());
                        }
                    }
                }
            } catch (InterruptedException e) {
                LOGGER.warn("[{}] {}", this.getId(), e.getMessage());
            }
        }
    }

    /**
     * Gets a string representation of a {@link SOAPMessage} for debugging
     * purposes.
     *
     * @param msg
     *            The message
     * @return The XML string.
     */
    private static String getXmlFromSOAPMessage(final SOAPMessage msg) {
        final ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        try {
            msg.writeTo(byteArrayOS);
        } catch (SOAPException | IOException e) {
            throw new SpException(e.getMessage());
        }
        return new String(byteArrayOS.toByteArray());
    }

    /**
     * Sends a SOAP message.
     *
     * @param msg
     *            The message.
     * @throws SOAPException
     *             When SOAP (connection) error.
     */
    private void sendMsg(final SmartschoolMsg msg) throws SOAPException {

        final SOAPMessage message =
                MessageFactory.newInstance().createMessage();

        final SOAPHeader header = message.getSOAPHeader();
        header.detachNode();

        final SOAPBody body = message.getSOAPBody();
        final QName bodyName = new QName(QNAME_SEND_MSG);
        final SOAPBodyElement bodyElement = body.addBodyElement(bodyName);

        bodyElement.addChildElement("accesscode")
                .addTextNode(String.valueOf(this.accesscode));
        bodyElement.addChildElement("senderIdentifier")
                .addTextNode(msg.getSender());
        bodyElement.addChildElement("userIdentifier")
                .addTextNode(msg.getRecipient());
        bodyElement.addChildElement("title").addTextNode(msg.getTitle());
        bodyElement.addChildElement("body").addTextNode(msg.getBody());

        //
        final SOAPMessage response =
                this.connection.call(message, this.endpointUrl);

        if (response == null) {
            throw new SOAPException(String.format(
                    "Smartschool [%s] response is null.", QNAME_SEND_MSG));
        }

        final SOAPBody responseBody = response.getSOAPBody();
        final SOAPBodyElement responseElement =
                (SOAPBodyElement) responseBody.getChildElements().next();
        final SOAPElement returnElement =
                (SOAPElement) responseElement.getChildElements().next();

        if (responseBody.getFault() != null) {
            throw new SOAPException(returnElement.getValue() + " "
                    + responseBody.getFault().getFaultString());
        }

        // Unexpected response
        if (!NumberUtils.isDigits(returnElement.getValue())) {
            LOGGER.error("[{}] {}", this.getId(),
                    getXmlFromSOAPMessage(response));
            return;
        }

        // Logical error?
        final int returnCode = Integer.valueOf(returnElement.getValue());

        if (returnCode == 0) {
            LOGGER.trace("[{}] {} from [{}] to [{}]", this.getId(),
                    QNAME_SEND_MSG, msg.getSender(), msg.getRecipient());
        } else {
            LOGGER.warn("[{}] {} from [{}] to [{}]: return [{}]", this.getId(),
                    QNAME_SEND_MSG, msg.getSender(), msg.getRecipient(),
                    returnCode);
        }
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void onStart() throws ServerPluginException {

        this.requestStop = false;

        try {
            this.connection =
                    SOAPConnectionFactory.newInstance().createConnection();

            this.eventConsumer = new EventConsumer(this);
            this.eventConsumer.start();

        } catch (UnsupportedOperationException | SOAPException e) {
            throw new ServerPluginException(e.getMessage());
        }
    }

    @Override
    public void onStop() throws ServerPluginException {

        this.requestStop = true;

        try {
            this.eventConsumer
                    .join(EventConsumer.MAX_MSECS_PROCESSING_AFTER_STOP);
        } catch (InterruptedException e) {
            LOGGER.warn("[{}] EventConsumer interrupted.", this.getId());
        }

        try {
            this.connection.close();
        } catch (SOAPException e) {
            LOGGER.warn("[{}] Error closing SOAP connection: {}", this.getId(),
                    e.getMessage());
        }
    }

    @Override
    public void onInit(final String pluginId, final String pluginName,
            final boolean live, final boolean online, final Properties props,
            final ServerPluginContext context) throws ServerPluginException {

        this.id = pluginId;
        this.name = pluginName;
        this.liveMode = live;
        this.pluginContext = context;

        if (!live) {
            LOGGER.warn("[{}] operates in [TEST] mode.", this.id);
        }
        //
        final String subDir = props.getProperty(PROP_TEMPLATE_HOME);

        if (subDir == null) {
            this.xmlResourceHome = context.getPluginHome();
        } else {
            final Path pathHome = Paths
                    .get(context.getPluginHome().getAbsolutePath(), subDir);
            if (!Files.isDirectory(pathHome)) {
                throw new ServerPluginException(String.format(
                        "%s [%s]: directory [%s] does not exist.",
                        PROP_TEMPLATE_HOME, subDir, pathHome.toString()));
            }
            this.xmlResourceHome = pathHome.toFile();
        }

        //
        this.xmlResourceJobTicketCancel =
                props.getProperty(PROP_TEMPLATE_JOBTICKET_CANCEL);

        this.xmlResourceJobTicketClose =
                props.getProperty(PROP_TEMPLATE_JOBTICKET_CLOSE);

        //
        this.senderGroup = props.getProperty(PROP_NOTIFICATION_SENDER_GROUP);
        this.recipientGroup =
                props.getProperty(PROP_NOTIFICATION_RECIPIENT_GROUP);

        //
        this.maxAcceptedEvents = null;

        final String maxEvents = props.getProperty(PROP_NOTIFICATION_EVENT_MAX);

        if (StringUtils.isNotBlank(maxEvents)) {
            if (NumberUtils.isDigits(maxEvents)) {
                this.maxAcceptedEvents = Integer.valueOf(maxEvents);
            } else {
                LOGGER.warn("Property [{}] value [{}] is invalid.",
                        PROP_NOTIFICATION_EVENT_MAX, maxEvents);
            }
        }

        //
        try {
            this.endpointUrl = new URL(String.format(ENDPOINT_STRING_FORMAT,
                    props.getProperty(PROP_ACCOUNT)));
        } catch (MalformedURLException e) {
            throw new ServerPluginException(e.getMessage());
        }

        this.accesscode = props.getProperty(PROP_PASSWORD).toCharArray();
    }

    /**
     * @param event
     *            The event.
     * @return Ticket data.
     */
    private static TemplateJobTicketDto
            createTemplateJobTicketDto(final JobTicketEvent event) {

        final TemplateJobTicketDto dto = new TemplateJobTicketDto();

        dto.setName(event.getDocumentName());
        dto.setNumber(event.getTicketNumber());
        dto.setOperator(event.getOperatorName());

        return dto;
    }

    /**
     * @param event
     *            The event.
     * @return User data.
     */
    private static TemplateUserDto
            createTemplateUserDto(final JobTicketEvent event) {

        final TemplateUserDto dto = new TemplateUserDto();

        dto.setFullName(event.getCreatorName());

        return dto;
    }

    /**
     * Put message on queue.
     *
     * @param msg
     *            The message.
     */
    private void putMsg(final SmartschoolMsg msg) {

        if (this.liveMode) {
            try {
                this.eventQueue.put(msg);
            } catch (InterruptedException e) {
                LOGGER.warn("[{}] {}", this.getId(), e.getMessage());
            }
        } else {
            final StringBuilder sb = new StringBuilder();
            sb.append("From: ").append(msg.getSender()).append("\n");
            sb.append("To: ").append(msg.getRecipient()).append("\n");
            sb.append("Title: ").append(msg.getTitle()).append("\n");
            sb.append("").append(msg.getBody());
            LOGGER.warn("[{}][TEST] Message\n{}", this.getId(), sb.toString());
        }
    }

    /**
     *
     * @param event
     *            The event.
     * @param ticketDto
     *            Ticket data.
     * @param userDto
     *            User data.
     * @param resourceName
     *            The name of the XML resource without the locale suffix and
     *            file extension.
     */
    private void putMsg(final JobTicketEvent event,
            final TemplateJobTicketDto ticketDto, final TemplateUserDto userDto,
            final String resourceName) {

        final SmartschoolMsg msg = new SmartschoolMsg();

        msg.setRecipient(event.getCreator());
        msg.setSender(event.getOperator());

        final JobTicketEventTemplate tmp = new JobTicketEventTemplate(
                this.xmlResourceHome, ticketDto, userDto);

        final JobTicketEventMessage eventMsg =
                tmp.render(resourceName, true, event.getLocale());

        msg.setTitle(eventMsg.getTitle());
        msg.setBody(eventMsg.getBody());

        this.putMsg(msg);
    }

    /**
     * Checks if ticket creator and operator are member of optional user group.
     *
     * @param event
     *            The ticket event.
     * @return {@code true} when no group constraints are present or ticket
     *         creator and operator are group member.
     */
    private boolean checkUserGroup(final JobTicketEvent event) {

        final boolean conditionA =
                checkUserGroup("Sender", this.senderGroup, event.getOperator());

        final boolean conditionB;

        // Trigger logging in test mode.
        if (conditionA || !this.liveMode) {
            conditionB = checkUserGroup("Recipient", this.recipientGroup,
                    event.getCreator());
        } else {
            conditionB = false;
        }
        return conditionA && conditionB;
    }

    /**
     * Checks if user is member of optional group.
     *
     * @param role
     *            Role name used for logging.
     * @param group
     *            User group id.
     * @param userid
     *            User id.
     * @return {@code true} when {@link #userGroupName} constraint is not
     *         present or userid is member of group.
     */
    private boolean checkUserGroup(final String role, final String group,
            final String userid) {

        final boolean isValid;

        if (StringUtils.isBlank(group)) {
            isValid = true;
        } else {
            isValid = this.pluginContext.isUserInGroup(group, userid);
        }

        if (!isValid && !this.liveMode) {
            LOGGER.warn(
                    "[{}][TEST] Job Ticket Notification: {} [{}] is "
                            + "NOT member of group [{}].",
                    this.getId(), role, userid, group);
        }
        return isValid;
    }

    @Override
    public boolean onJobTicketEvent(final JobTicketCancelEvent event,
            final NotificationEventProgress progress) {

        if (!acceptEvent(event, progress)) {
            return false;
        }

        final TemplateJobTicketDto ticketDto =
                createTemplateJobTicketDto(event);

        ticketDto.setReturnMessage(Objects.toString(event.getReason(), "-"));

        this.putMsg(event, ticketDto, createTemplateUserDto(event),
                this.xmlResourceJobTicketCancel);

        return true;
    }

    @Override
    public boolean onJobTicketEvent(final JobTicketCloseEvent event,
            final NotificationEventProgress progress) {

        if (!acceptEvent(event, progress)) {
            return false;
        }

        this.putMsg(event, createTemplateJobTicketDto(event),
                createTemplateUserDto(event), this.xmlResourceJobTicketClose);

        return true;
    }

    /**
     *
     * @param event
     *            The {@link JobTicketEvent}.
     * @param progress
     *            The {@link NotificationEventProgress}.
     * @return {@code true} when event is accepted.
     */
    private boolean acceptEvent(final JobTicketEvent event,
            final NotificationEventProgress progress) {

        if (this.maxAcceptedEvents != null && progress.acceptedEvents(
                this.getClass()) >= this.maxAcceptedEvents.intValue()) {
            return false;
        }

        if (!checkUserGroup(event)) {
            return false;
        }

        return true;
    }

}
